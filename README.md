# DVT: Mobile Developer Airport Application
#### To Create an airport application to display the nearby airports based on your current location and when a location is clicked on it will display the departure flights for that airport.

## Requirements:
1. You are required to implement the designs that are seen below. Assets will be
attached below.
2. The airports displayed must be based on the userís current location.
3. TheapplicationshouldconnecttothefollowingAPItocollecttheAPI'sinformation.
Nearby Airport API - https://aviation-edge.com/nearby-airport-and-city-api/ (
Endpoint example : GET http://aviation-
edge.com/api/public/nearby?key=api_key&lat=0.0&lng=0.0&distance=1) Airport Schedule API - https://aviation-edge.com/flight-schedule-and-timetable-of- airlines-and-airports/ (Endpoint example : GET http://aviation- edge.com/api/public/timetable?key=api_key&iataCode=JFK&type=departure )
4. For the Airport Schedule you will receive the IATA code from the Nearby Airport
API call. This means these API calls go hand in hand to create a complete app )
5. Please make sure to handle any errors also that might be received these API calls in
a graceful manner.
6. Developer can complete the application in either Android or iOS.

It is up to the developer to solve this problem using any technology/libraries you want to use.
DVT recommends following good coding principles and architecture. DVT will be
assessing not just your ability to code, but the developer's coding style as well. Developer to make
sure he/she submits his best attempt at the problem Ė he/she should not be afraid to show off if time permits!

Developer to upload the complete solution to a Git repository hosted on either Github, Bitbucket or GitLab and send through a public link to the solution and a downloadable app file to Rose Allen (RAllen@jhb.dvt.co.za).