package com.dvt.eme.airport.airport_api_daolayer.models;

import java.io.Serializable;
import java.util.ArrayList;

public class AppNearByAirports implements Serializable {
    private ArrayList<NearByAirport> nearByAirports;

    public ArrayList<NearByAirport> getNearByAirports() {
        return nearByAirports;
    }

    public void setNearByAirports(ArrayList<NearByAirport> nearByAirports) {
        this.nearByAirports = nearByAirports;
    }
}
