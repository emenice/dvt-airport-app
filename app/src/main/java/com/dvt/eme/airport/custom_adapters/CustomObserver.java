package com.dvt.eme.airport.custom_adapters;

import com.dvt.eme.airport.airport_api_daolayer.models.NearByAirport;

public interface CustomObserver {
    void update(NearByAirport nearByAirport);
}
