package com.dvt.eme.airport.services;

import android.os.IBinder;
import com.google.android.gms.maps.model.LatLng;

public interface ILocationService extends IBinder {
    void getNearByAirport(LatLng mLocation, Double distance);
}
