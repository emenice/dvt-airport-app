package com.dvt.eme.airport.ui_controllers

import android.content.*
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.location.LocationManager
import android.location.LocationManager.GPS_PROVIDER
import android.os.Bundle
import android.provider.ContactsContract
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Button
import android.widget.LinearLayout
import android.widget.ListView
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.coderfolk.multilamp.customView.MultiLamp
import com.coderfolk.multilamp.customView.MultiLamp.TOP
import com.coderfolk.multilamp.model.Target
import com.coderfolk.multilamp.shapes.Circle
import com.coderfolk.multilamp.shapes.Rectangle
import com.dvt.eme.airport.R
import com.dvt.eme.airport.airport_api_daolayer.models.NearByAirport
import com.dvt.eme.airport.custom_adapters.CustomObserver
import com.dvt.eme.airport.custom_adapters.NearByAiportsListViewAdapter
import com.dvt.eme.airport.helpers.PersistenceHelper
import com.dvt.eme.airport.helpers.PersistenceHelper.getTutorialOverlayRunOnce
import com.dvt.eme.airport.helpers.PersistenceHelper.setTutorialOverlayRunOnce
import com.dvt.eme.airport.services.NearByAirportsService
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*

class NearByAirportsActivity : AppCompatActivity(), OnMapReadyCallback , CustomObserver{

    private val TAG = NearByAirportsActivity::class.java!!.getSimpleName()
    val PACKAGE_NAME = "com.dvt.eme.airport"

    private lateinit var mMap: GoogleMap
    private lateinit var mCameraPosition: CameraPosition


    // The entry point to the Fused Location Provider.
    private lateinit var mFusedLocationProviderClient: FusedLocationProviderClient

    // A default location (Durban, South Africa
    val mDefaultLocation = LatLng(-29.8192, 31.0096)
    private val DEFAULT_ZOOM = 10.0f
    private val DEFAULT_RADIUS: Double = 20000.0
    private val DEFAULT_COLOR = Color.LTGRAY
    private val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1
    private var mLocationPermissionGranted: Boolean = false

    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
    private var mLastKnownLocation: Location? = null


    // BroadcastReceiver receiver for NearByAirportsService
    private lateinit var nearByAirportsBroadcastReceiver: BroadcastReceiver
    lateinit var nearByAirportList: ArrayList<NearByAirport>
    lateinit var listView :ListView
    lateinit var multiLamp: MultiLamp

    var mLocationManager: LocationManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Retrieve location and camera position from saved instance state.
//        if (savedInstanceState != null) {
//            mLastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION)
//            mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION)
//        }
        setContentView(R.layout.activity_near_by_airports)

        mLocationManager = getSystemService(LOCATION_SERVICE) as LocationManager?
        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        listView = findViewById<ListView>(R.id.list_view_nearby_airports)


        nearByAirportsBroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                if (intent.action.equals(PACKAGE_NAME)) {
                    nearByAirportList = intent.getSerializableExtra("value") as ArrayList<NearByAirport>
                    showListViewController()
                }
                nearByAirportList.forEach { it ->
                    mMap.addMarker(
                        MarkerOptions().title(it.nameAirport)
                            .position(LatLng(it.latitudeAirport.toDouble(), it.longitudeAirport.toDouble())))
                }
                addOnInfoWindowClickLister()
            }
        }
    }

    fun onClickShowNearByAirportsList(view :View) {
        view.visibility = GONE
        if(!nearByAirportList.isEmpty()){
            showNearbyAirportsListView()
        }
    }

    private fun showListViewController() {
        val showListTextView = findViewById<Button>(R.id.btn_showlist_nearby_airports)
        showListTextView.visibility = VISIBLE

        if(!nearByAirportList.isEmpty()){
            showListTextView.text = getString(R.string.showlist)
            startTutorialOverlay()
        }
    }

    private fun showNearbyAirportsListView() {
        listView.visibility = VISIBLE
        showListViewTutorialOverlay()
        val nearByAirportAdapter = NearByAiportsListViewAdapter(this@NearByAirportsActivity,this@NearByAirportsActivity ,nearByAirportList)
        listView.adapter = nearByAirportAdapter

    }

    private fun addOnInfoWindowClickLister() {
        mMap.setOnInfoWindowClickListener(object : GoogleMap.OnInfoWindowClickListener {
            override fun onInfoWindowClick(marker: Marker?) {
                Log.d(TAG, "Airport name: " + marker!!.title + " Location: " + marker.position.toString())
                try {
                    val chosenNearByAirport = nearByAirportList.filter {
                        it.nameAirport.equals(marker!!.title) && it.latitudeAirport.equals(marker.position.latitude
                            .toString())}.get(0)
                    startAirlineTimetableActivity(chosenNearByAirport)
                } catch (e: Exception) {
                    Log.d(TAG, e.printStackTrace().toString())
                    showError("Retry & select another Airport", false)
                }
            }
        })
    }

    override fun update(nearByAirport: NearByAirport?) {
        startAirlineTimetableActivity(nearByAirport)

    }

    private fun startAirlineTimetableActivity(selectedAirport: NearByAirport?) {
        val intent = Intent(this@NearByAirportsActivity, AirportTimetableActivity::class.java)
        intent.putExtra("chosenAirport", selectedAirport)
        stopNearByAirportService()
        startActivity(intent)
    }
    private fun showError(errorMessage: String, logout: Boolean) {

        if(logout){
            val alert = AlertDialog.Builder(this)
            alert.setTitle("GPS Settings or Permissions Disabled")
//            alert.setMessage("Please enable GPS settings or accept Permissions, and restart the application")
            alert.setMessage(errorMessage)
            alert.setPositiveButton("OK", object: DialogInterface.OnClickListener {
                /**
                 * This method will be invoked when a button in the dialog is clicked.
                 *
                 * @param dialog the dialog that received the click
                 * @param which the button that was clicked (ex.
                 * [DialogInterface.BUTTON_POSITIVE]) or the position
                 * of the item clicked
                 */
                override fun onClick(dialog: DialogInterface?, which: Int) {
                    dialog?.cancel()
                    finish()
                }
            })
            alert.create().show()
        }else {
            Toast.makeText(this@NearByAirportsActivity.applicationContext, errorMessage, Toast.LENGTH_SHORT).show();
        }
    }

    private fun stopNearByAirportService() {

        if (nearByAirportsBroadcastReceiver != null) {
            try {
                unregisterReceiver(nearByAirportsBroadcastReceiver)
                stopService(Intent(this@NearByAirportsActivity, NearByAirportsService::class.java))
            } catch (e: Exception) {
                Log.d(TAG, e.printStackTrace().toString())
                //just ignore
            }
        }
    }

    override fun onPause() {
        super.onPause()
        stopNearByAirportService()
    }

    override fun onResume() {
        super.onResume()
        if (nearByAirportsBroadcastReceiver != null) {
            registerReceiver(nearByAirportsBroadcastReceiver, IntentFilter(PACKAGE_NAME))
        }
    }


//    /**
//     * Saves the state of the map when the activity is paused.
//     */
//    override fun onSaveInstanceState(outState: Bundle) {
//        if (mMap != null) {
//            outState.putParcelable(KEY_CAMERA_POSITION, mMap.cameraPosition)
//            outState.putParcelable(KEY_LOCATION, mLastKnownLocation)
//            super.onSaveInstanceState(outState)
//        }
//    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker showing intended location and a long press will search for nearby Airports and
     * put markers in the respective circle.
     * By default a circle is centered around current user location
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap ?: return
        // Prompt the user for permission.
        getLocationPermission()
        with(mMap) {
            setOnMapLongClickListener { point ->
                this.clear()
                val view: View =
                    supportFragmentManager.findFragmentById(R.id.map)?.view ?: return@setOnMapLongClickListener
                // made radius dynamic to ensure that when one zooms out one can search for a bigger area
                // (radius is fixed as per the view but dynamic in meters)
                val radius = determineRadiusBasedOnView(this)
                drawCircle(point, radius, this)
                startNearByAirpotsService(point, radius / 1000) // converting radius into km
                try {
                    if (!listView.adapter.isEmpty) {
                        listView.visibility = GONE
                    }
                }catch(e : Exception){
                    Log.d(TAG, e.printStackTrace().toString())
                }
            }
        }
    }

    private fun startTutorialOverlay() {
        if(getTutorialOverlayRunOnce(this@NearByAirportsActivity.applicationContext)){
            val btnNearByAirports = findViewById(R.id.btn_showlist_nearby_airports) as Button
            val guideline = findViewById<LinearLayout>(R.id.ll_invisible)
            multiLamp = MultiLamp(this)
            val targets = java.util.ArrayList<Target>()
            targets.add(Target(btnNearByAirports, getString(R.string.start_up_description), TOP, Rectangle()))
            targets.add(Target(guideline, getString(R.string.long_press_description),TOP, Circle(10.0f)))
            multiLamp.build(targets)
            multiLamp.addCallback {
                setTutorialOverlayRunOnce(this@NearByAirportsActivity.applicationContext, false)
            }
        }
    }
    private fun showListViewTutorialOverlay() {
        if(PersistenceHelper.getlistViewTutorialOverlayRunOnce(this@NearByAirportsActivity.applicationContext)){
            val listViewNearByAirports = findViewById(R.id.list_view_nearby_airports) as ListView
            val guideline = findViewById<LinearLayout>(R.id.ll_invisible)
            multiLamp = MultiLamp(this)
            val targets = java.util.ArrayList<Target>()
            targets.add(Target(listViewNearByAirports, getString(R.string.list_view_description), TOP, Rectangle()))
            targets.add(Target(guideline, getString(R.string.long_press1_description),TOP, Circle(10.0f)))
            multiLamp.build(targets)
            multiLamp.addCallback {
                PersistenceHelper.setlistViewTutorialOverlayRunOnce(this@NearByAirportsActivity.applicationContext, false)
            }
        }
    }

    private fun determineRadiusBasedOnView(googleMap: GoogleMap): Double {
        val bounds :LatLngBounds = googleMap.projection.visibleRegion.latLngBounds
        val llNeLat: Double = bounds.northeast.latitude
        val llSwLat: Double = bounds.southwest.latitude
        val llNeLng: Double = bounds.northeast.longitude
        val llSwLng: Double = bounds.southwest.longitude
        val results = FloatArray(5)
        Location.distanceBetween(llNeLat, llNeLng, llSwLat, llSwLng, results)
        return results[0].toDouble()/4
    }

    private fun drawCircle(center: LatLng?, radius: Double, googleMap: GoogleMap) {
        googleMap.addCircle(CircleOptions().center(center).radius(radius).fillColor(DEFAULT_COLOR).strokeColor(Color.TRANSPARENT))
    }

    private fun startNearByAirpotsService(center: LatLng, radius: Double) {
        val intent = Intent(this@NearByAirportsActivity, NearByAirportsService::class.java)

        intent.putExtra("distance", radius)
        intent.putExtra("longitude", center.longitude)
        intent.putExtra("latitude", center.latitude)
        startService(intent)
        registerReceiver(nearByAirportsBroadcastReceiver, IntentFilter(PACKAGE_NAME))
    }

    /**
     * Gets the current location of the device, and positions the map's camera.
     */
    private fun getDeviceLocation() {
        /*
        * Get the best and most recent location of the device, which may be null in rare
        * cases when a location is not available.
        */
        var location: LatLng
        try {
            if (mLocationPermissionGranted) {
                val locationResult = mFusedLocationProviderClient.lastLocation
                locationResult.addOnCompleteListener(this) { task ->

                    if (task.isSuccessful) {
                        // Set the map's camera position to the current location of the device.
                        mLastKnownLocation = task.result
                        location = LatLng(mLastKnownLocation?.getLatitude()?:mDefaultLocation.latitude, mLastKnownLocation?.getLongitude()?:mDefaultLocation.longitude)
                    } else {
                        Log.d(TAG, "Current location is null. Using defaults.")
                        Log.e(TAG, "Exception: %s", task.exception)
                        location = mDefaultLocation
                        mMap.uiSettings.isMyLocationButtonEnabled = false
                    }
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, DEFAULT_ZOOM))
                    drawCircle(location, DEFAULT_RADIUS, mMap)
                    startNearByAirpotsService(location, DEFAULT_RADIUS / 1000)
                }
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message)
        }
    }

    /**
     * Prompts the user for permission to use the device location.
     */
    private fun getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.applicationContext,
                android.Manifest.permission.ACCESS_FINE_LOCATION) === PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true
            mMap.isMyLocationEnabled = true
            if(mLocationManager!!.isProviderEnabled(GPS_PROVIDER)) {
                getDeviceLocation()
            } else {
                showError("DvtAirportApp requires GPS to be enabled for it to function", true)
            }
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
        }
    }

    /**
     * Handles the result of the request for location permissions.
     */
    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        mLocationPermissionGranted = false
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true
                    if(mLocationManager!!.isProviderEnabled(GPS_PROVIDER)) {
                        getDeviceLocation()
                    } else {
                        showError("DvtAirportApp requires GPS to be enabled for it to function", true)
                        //considering to logout
                    }
                }
            }
        }
        updateLocationUI()
    }

    /**
     * Updates the map's UI settings based on whether the user has granted location permission.
     */
    private fun updateLocationUI() {
        if (mMap == null) {
            return
        }
        try {
            if (mLocationPermissionGranted) {
                mMap.isMyLocationEnabled = true
                mMap.uiSettings.isMyLocationButtonEnabled = true
            } else {
                mMap.isMyLocationEnabled = false
                mMap.uiSettings.isMyLocationButtonEnabled = false
                mLastKnownLocation = null
                showError("DVTAirportApp cannot be used without accessing the device's location", true)
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message)
        }

    }
}

/**
 * Extension function to find the distance from this to another LatLng object
 */
private fun LatLng.distanceFrom(latLng: LatLng): Double {
    val result = FloatArray(1)
    Location.distanceBetween(latitude, longitude, latLng.latitude, latLng.longitude, result)
    return result[0].toDouble()
}
