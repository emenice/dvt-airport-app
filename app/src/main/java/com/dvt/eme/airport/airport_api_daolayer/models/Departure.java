
package com.dvt.eme.airport.airport_api_daolayer.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Departure {

    @SerializedName("iataCode")
    @Expose
    private String iataCode;
    @SerializedName("icaoCode")
    @Expose
    private String icaoCode;
    @SerializedName("scheduledTime")
    @Expose
    private String scheduledTime;
    @SerializedName("estimatedRunway")
    @Expose
    private String estimatedRunway;
    @SerializedName("actualRunway")
    @Expose
    private String actualRunway;
    @SerializedName("gate")
    @Expose
    private String gate;
    @SerializedName("estimatedTime")
    @Expose
    private String estimatedTime;
    @SerializedName("actualTime")
    @Expose
    private String actualTime;
    @SerializedName("terminal")
    @Expose
    private String terminal;

    public String getIataCode() {
        return iataCode;
    }

    public void setIataCode(String iataCode) {
        this.iataCode = iataCode;
    }

    public String getIcaoCode() {
        return icaoCode;
    }

    public void setIcaoCode(String icaoCode) {
        this.icaoCode = icaoCode;
    }

    public String getScheduledTime() {
        return scheduledTime;
    }

    public void setScheduledTime(String scheduledTime) {
        this.scheduledTime = scheduledTime;
    }

    public String getEstimatedRunway() {
        return estimatedRunway;
    }

    public void setEstimatedRunway(String estimatedRunway) {
        this.estimatedRunway = estimatedRunway;
    }

    public String getActualRunway() {
        return actualRunway;
    }

    public void setActualRunway(String actualRunway) {
        this.actualRunway = actualRunway;
    }

    public String getGate() {
        return gate;
    }

    public void setGate(String gate) {
        this.gate = gate;
    }

    public String getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(String estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public String getActualTime() {
        return actualTime;
    }

    public void setActualTime(String actualTime) {
        this.actualTime = actualTime;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

}
