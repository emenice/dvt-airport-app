package com.dvt.eme.airport.ui_controllers

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.ListView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.dvt.eme.airport.R
import com.dvt.eme.airport.airport_api_daolayer.models.AirlineTimetableItem
import com.dvt.eme.airport.airport_api_daolayer.models.NearByAirport
import com.dvt.eme.airport.airport_api_daolayer.restclient.DataWrapper
import com.dvt.eme.airport.custom_adapters.AirportTimetableAdapter
import com.dvt.eme.airport.view_models.AirportTimetableActivityViewModelFactory
import com.dvt.eme.airport.view_models.AirportTimetableViewModel

class AirportTimetableActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_airport_timetable)
        setSupportActionBar(this.findViewById(R.id.action_bar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        val chosenAirport: NearByAirport = intent.getSerializableExtra("chosenAirport") as NearByAirport
        findViewById<TextView>(R.id.tv_airport_name).text = chosenAirport.nameAirport
        findViewById<TextView>(R.id.tv_airport_description).text = getString(R.string.airport_placeholder)+ " " +chosenAirport.nameCountry

        val model = ViewModelProviders.of(this, AirportTimetableActivityViewModelFactory(this,
            chosenAirport.codeIataAirport, "departure")).get(AirportTimetableViewModel::class.java)

        val listView = findViewById(R.id.list_view_timetable) as ListView
        val progressBar = findViewById<ProgressBar>(R.id.progressbar)
        progressBar.visibility = View.VISIBLE

        model.airlineTimetableList.observe(this, Observer<DataWrapper<List<AirlineTimetableItem>>> { airlimeTimetableItems ->
            //update UI
            if(airlimeTimetableItems.data!=null) {
                val airlineTimetableAdapter = AirportTimetableAdapter(this, airlimeTimetableItems.data)
                listView.adapter = airlineTimetableAdapter
            } else{
                findViewById<TextView>(R.id.tv_airport_timetable_status).visibility = View.VISIBLE
            }
            progressBar.visibility = View.GONE
        })
//        model.airlineTimetableList.observe(this, object: Observer<DataWrapper<List<AirlineTimetableItem>>>{
//            /**
//             * Called when the data is changed.
//             * @param t  The new data
//             */
//            override fun onChanged(t: DataWrapper<List<AirlineTimetableItem>>?) {
//                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//            }
//        })

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
