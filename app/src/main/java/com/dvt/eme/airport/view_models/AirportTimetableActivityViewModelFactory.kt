package com.dvt.eme.airport.view_models

import android.app.Application
import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class AirportTimetableActivityViewModelFactory (private val context: Context, private val iataCode : String = "DUR",
                                                private val type : String ="departure") : ViewModelProvider.NewInstanceFactory(){
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AirportTimetableViewModel(context, iataCode, type) as T
    }
}