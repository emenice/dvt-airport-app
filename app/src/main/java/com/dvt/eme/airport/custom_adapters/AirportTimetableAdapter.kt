package com.dvt.eme.airport.custom_adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.dvt.eme.airport.R
import com.dvt.eme.airport.airport_api_daolayer.models.AirlineTimetableItem
import java.text.SimpleDateFormat
import java.util.*

class AirportTimetableAdapter(val context: Context, val airlineTimetableItems: List<AirlineTimetableItem>): BaseAdapter() {

    /**
     * Get a View that displays the data at the specified position in the data set. You can either
     * create a View manually or inflate it from an XML layout file. When the View is inflated, the
     * parent View (GridView, ListView...) will apply default layout parameters unless you use
     * [android.view.LayoutInflater.inflate]
     * to specify a root view and to prevent attachment to the root.
     *
     * @param position The position of the item within the adapter's data set of the item whose view
     * we want.
     * @param convertView The old view to reuse, if possible. Note: You should check that this view
     * is non-null and of an appropriate type before using. If it is not possible to convert
     * this view to display the correct data, this method can create a new view.
     * Heterogeneous lists can specify their number of view types, so that this View is
     * always of the right type (see [.getViewTypeCount] and
     * [.getItemViewType]).
     * @param parent The parent that this view will eventually be attached to
     * @return A View corresponding to the data at the specified position.
     */
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val rowView = inflater.inflate(R.layout.airport_timetable_list_item, parent, false)
        val airlineTimetableItem : AirlineTimetableItem  = getItem(position)
        val textViewAirlineName = rowView.findViewById<TextView>(R.id.tv_airline_name);
        val textViewAirlineStatus = rowView.findViewById<TextView>(R.id.tv_airline_status)
        val textViewDepartureTime =  rowView.findViewById<TextView>(R.id.tv_departure_time)
        val textViewFlightNumber = rowView.findViewById<TextView>(R.id.tv_flight_number)
        val textViewDestination = rowView.findViewById<TextView>(R.id.tv_destination)

        textViewAirlineName.text = airlineTimetableItem.airline.name
        textViewAirlineStatus.text = airlineTimetableItem.status
        if (getAirplaneStatus(airlineTimetableItem.status)){
            textViewAirlineStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.green_dot_x1,0,0,0)
        }

        textViewDepartureTime.text = formartDate(airlineTimetableItem.departure.scheduledTime) //todo: add logic to determine which time to pic
        textViewFlightNumber.text = airlineTimetableItem.flight.iataNumber
        textViewDestination.text = airlineTimetableItem.arrival.iataCode //todo: determine name of city from the iata code
        return rowView
    }

    private fun getAirplaneStatus(status: String): Boolean {
        if(status.equals("scheduled") || status.equals("active")) {
            return true
        }
        return false
    }

    private fun formartDate(scheduledTime: String?): CharSequence? {
        val dateFormat = SimpleDateFormat("yyyy-mm-dd'T'HH:mm:ss.SSS")
        var time = SimpleDateFormat("HH:mm").format(dateFormat.parse(scheduledTime))

        return time.toString()
    }

    /**
     * Get the data item associated with the specified position in the data set.
     *
     * @param position Position of the item whose data we want within the adapter's
     * data set.
     * @return The data at the specified position.
     */
    override fun getItem(position: Int): AirlineTimetableItem {
        return airlineTimetableItems[position]
    }

    /**
     * Get the row id associated with the specified position in the list.
     *
     * @param position The position of the item within the adapter's data set whose row id we want.
     * @return The id of the item at the specified position.
     */
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    /**
     * How many items are in the data set represented by this Adapter.
     *
     * @return Count of items.
     */
    override fun getCount(): Int {
        return airlineTimetableItems.size
    }
}