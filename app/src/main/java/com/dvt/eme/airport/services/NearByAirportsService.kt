package com.dvt.eme.airport.services

import android.app.Service
import android.content.Intent
import android.util.Log
import android.widget.Toast
import com.dvt.eme.airport.R
import com.dvt.eme.airport.airport_api_daolayer.models.NearByAirport
import com.dvt.eme.airport.airport_api_daolayer.restclient.AirportApiService
import com.dvt.eme.airport.airport_api_daolayer.restclient.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.net.UnknownHostException
import java.util.ArrayList
import java.util.concurrent.TimeoutException

class NearByAirportsService : Service() {
    private val TAG = NearByAirportsService::class.java!!.getSimpleName()
    val PACKAGE_NAME = "com.dvt.eme.airport"
    lateinit var nearByAirportsList: ArrayList<NearByAirport>

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
//        return super.onStartCommand(intent, flags, startId)

        if (intent != null) {
            var distance = intent.getDoubleExtra("distance", 0.0)
            val longitude = intent.getDoubleExtra("longitude", 0.0)
            val latitude = intent.getDoubleExtra("latitude", 0.0)
////            Limiting range to get less items
//            if (distance > 100.0) {
//                distance = 100.0
//            }
            getNearByAirports(latitude.toString(), longitude.toString(), distance)
        }
        return START_STICKY
    }

    override fun onBind(intent: Intent): ILocationService {
        TODO("Return the communication channel to the service.")
    }

    fun getNearByAirports(latitude: String, longitude: String, distance: Double) {
        val airportApiService = RetrofitClient.getRetrofitInstance(this.getString(R.string.base_url)).create(
            AirportApiService::class.java
        )

        airportApiService.findNearByAirports(this.getString(R.string.api_key), latitude, longitude, distance)
            .enqueue(object : Callback<ArrayList<NearByAirport>> {
                override fun onResponse(
                    call: Call<ArrayList<NearByAirport>>,
                    response: Response<ArrayList<NearByAirport>>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        nearByAirportsList = response.body()!!
                        sendBroadcastIntent()
                    } else {
                        //todo: to filter through all the possible failures here
                        Toast.makeText(
                            this@NearByAirportsService.applicationContext, "Server returned a response, " +
                                    "just not what we looking for.", Toast.LENGTH_SHORT
                        ).show();
                    }
                }

                override fun onFailure(call: Call<ArrayList<NearByAirport>>, t: Throwable) {
                    Log.d(TAG, t.localizedMessage)
                    if (t is TimeoutException) {
                        Toast.makeText(
                            this@NearByAirportsService.applicationContext,
                            "Socket timeout error, possibly retry",
                            Toast.LENGTH_SHORT
                        ).show();
//                    } else if(t is UnknownHostException){
//                        Toast.makeText(this@NearByAirportsService.applicationContext, t.message, Toast.LENGTH_SHORT).show();
                    } else if (t is IOException) {
                        Toast.makeText(
                            this@NearByAirportsService.applicationContext,
                            "this is an actual network failure: possibly check data connection & retry",
                            Toast.LENGTH_SHORT
                        ).show();
                   } else {
//                    Toast.makeText(this@NearByAirportsService.applicationContext, "conversion issue! big problems :(", Toast.LENGTH_SHORT).show();
                        Toast.makeText(
                            this@NearByAirportsService.applicationContext,
                            "No Airports available in chosen area, zoom out and select area!!",
                            Toast.LENGTH_SHORT
                        ).show();

                    }
                }
            })
    }

    private fun sendBroadcastIntent() {
        val intent = Intent(PACKAGE_NAME)
        intent.putExtra("value", nearByAirportsList)
        sendBroadcast(intent)
    }
}
