package com.dvt.eme.airport.airport_api_daolayer.restclient;


import com.dvt.eme.airport.airport_api_daolayer.models.AirlineTimetableItem;
import com.dvt.eme.airport.airport_api_daolayer.models.NearByAirport;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


import java.util.ArrayList;
import java.util.List;

public interface AirportApiService {

    @GET("v2/public/nearby")
    Call<ArrayList<NearByAirport>> findNearByAirports(@Query("key") String apiKey, @Query("lat") String latitude,
                                                      @Query("lng") String longitude, @Query("distance") double distance);

    @GET("v2/public/timetable")
    Call<List<AirlineTimetableItem>> retrieveAirlineTimetable(@Query("key") String apiKey, @Query("iataCode") String iataCode,
                                                                   @Query("type") String type);
}
