package com.dvt.eme.airport.view_models;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.dvt.eme.airport.R;
import com.dvt.eme.airport.airport_api_daolayer.restclient.AirportApiService;
import com.dvt.eme.airport.airport_api_daolayer.restclient.DataWrapper;
import com.dvt.eme.airport.airport_api_daolayer.restclient.RetrofitClient;
import com.dvt.eme.airport.airport_api_daolayer.models.AirlineTimetableItem;
import org.jetbrains.annotations.NotNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class AirportTimetableViewModel extends ViewModel {
    private String TAG = AirportTimetableViewModel.class.getSimpleName();
    private MutableLiveData<DataWrapper<List<AirlineTimetableItem>>> airlineTimetableList;
    private DataWrapper<List<AirlineTimetableItem>> dataWrapper = new DataWrapper<>();
    private Context context;
    private String iataCode;
    private String type;
    public LiveData<DataWrapper<List<AirlineTimetableItem>>> getAirlineTimetableList(){

        if(airlineTimetableList == null){
            airlineTimetableList = new MutableLiveData<>();
            loadAirtimeTimetables();
        }
        return airlineTimetableList;
    }

    public AirportTimetableViewModel(@NotNull Context context, String iataCode, String type){
        this.context = context.getApplicationContext();
        this.iataCode = iataCode;
        this.type = type;
    };

    private void loadAirtimeTimetables() {
        AirportApiService airportApiService = RetrofitClient.getRetrofitInstance(this.context.getString(R.string.base_url)).create(AirportApiService.class);
        airportApiService.retrieveAirlineTimetable(this.context.getString(R.string.api_key), this.iataCode, this.type).enqueue(new Callback<List<AirlineTimetableItem>>() {
            @Override
            public void onResponse(Call<List<AirlineTimetableItem>> call, Response<List<AirlineTimetableItem>> response) {
                if(response.isSuccessful() && response.body()!= null) {
                    dataWrapper.setData(response.body());
                    airlineTimetableList.setValue(dataWrapper);
                }
            }

            @Override
            public void onFailure(Call<List<AirlineTimetableItem>> call, Throwable t) {
                if(t instanceof TimeoutException) {
                    dataWrapper.setApiExcption(new TimeoutException("Timeout Exception"));
                    Toast.makeText(
                            context.getApplicationContext(),
                    "Socket timeout error, possibly retry",
                            Toast.LENGTH_SHORT
                        ).show();
//                    } else if(t instanceof UnknownHostException){
//                        Toast.makeText(context.getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (t instanceof IOException) {
                    dataWrapper.setApiExcption(new IOException("IO Exception"));
                    Toast.makeText(
                            context.getApplicationContext(),
                    "this is an actual network failure: possibly check data connection & retry",
                            Toast.LENGTH_SHORT
                        ).show();
                } else {
                    dataWrapper.setApiExcption(new Exception("Parsing Exception"));
                    Toast.makeText(
                            context.getApplicationContext(),
                    "No publicly viewed timetable is available for specific Airport, choose another!!",
                            Toast.LENGTH_SHORT
                        ).show();
                }

                airlineTimetableList.setValue(dataWrapper);
            }
        });
    }
    @Override
    protected void onCleared() {
        super.onCleared();
        Log.d(TAG, "on cleared called");
    }

}
