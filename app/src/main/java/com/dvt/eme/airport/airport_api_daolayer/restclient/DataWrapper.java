package com.dvt.eme.airport.airport_api_daolayer.restclient;

public class DataWrapper<T> {
    private Exception apiExcption;
    private T data;

    public Exception getApiExcption() {
        return apiExcption;
    }

    public void setApiExcption(Exception apiExcption) {
        this.apiExcption = apiExcption;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
