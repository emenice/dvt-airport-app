
package com.dvt.eme.airport.airport_api_daolayer.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Codeshared {

    @SerializedName("airline")
    @Expose
    private Airline_ airline;
    @SerializedName("flight")
    @Expose
    private Flight_ flight;

    public Airline_ getAirline() {
        return airline;
    }

    public void setAirline(Airline_ airline) {
        this.airline = airline;
    }

    public Flight_ getFlight() {
        return flight;
    }

    public void setFlight(Flight_ flight) {
        this.flight = flight;
    }

}
