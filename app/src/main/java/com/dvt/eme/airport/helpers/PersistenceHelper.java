package com.dvt.eme.airport.helpers;

import android.content.Context;
import android.content.SharedPreferences;

public class PersistenceHelper {
    private static final String PERSISTENT_HELPER_PREFS_NAME = "persistenthelper_preferences";
    public static boolean getlistViewTutorialOverlayRunOnce(Context ctx) {
        SharedPreferences sharedPreferences;
        sharedPreferences = ctx.getSharedPreferences(PERSISTENT_HELPER_PREFS_NAME, 0);
        return sharedPreferences.getBoolean("listViewTutorialRunOnce", true);
    }

    public static void setlistViewTutorialOverlayRunOnce(Context ctx, boolean var){
        SharedPreferences sharedPreferences;
        sharedPreferences = ctx.getSharedPreferences(PERSISTENT_HELPER_PREFS_NAME, 0);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("listViewTutorialRunOnce", var);
        editor.commit();
    }
    public static void setTutorialOverlayRunOnce (Context ctx, boolean var){
        SharedPreferences sharedPreferences;
        sharedPreferences = ctx.getSharedPreferences(PERSISTENT_HELPER_PREFS_NAME, 0);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("tutorialOverlayRunOnce", var);
        editor.commit();
    }
    public static boolean getTutorialOverlayRunOnce(Context ctx) {
        SharedPreferences sharedPreferences;
        sharedPreferences = ctx.getSharedPreferences(PERSISTENT_HELPER_PREFS_NAME, 0);
        return sharedPreferences.getBoolean("tutorialOverlayRunOnce", true);
    }
}
